# INW PAYLOAD ENCODER

__Table of contents__

- [INW-PAYLOAD-ENCODER](#path-logger)
  - [Installation](#installation)
  - [Usage](#usage)

## Installation

`inw-playload-encoder` can be installed on Linux, Mac OS or Windows without any issues.

Install via NPM

```
npm install inw-payload-encoder --save 
```

## Usage
1. Import

```javascript
import payloadEncode from 'inw-payload-encoder';
```

2. Encode Image

```javascript
let someObject = {
    msg: "Life would be tragic if it weren’t funny."
}
let objPayload = payloadEncode(someObject);
/* OUTPUT
    {
        payload: "some-hash"
    }
*/
```