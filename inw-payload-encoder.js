function payloadEncode(source){
    return `{"payload": "${btoa(unescape(encodeURIComponent(JSON.stringify(source))))}"}`;
}
export default payloadEncode;